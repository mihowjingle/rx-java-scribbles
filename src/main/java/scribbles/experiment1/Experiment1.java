package scribbles.experiment1;

import io.reactivex.rxjava3.core.Observable;

public class Experiment1 {
    public static void main(String[] args) {
        Observable.<Integer>create(emitter -> {
            for (int i = 0; i < 1000; i++) {
                emitter.onNext(i);
            }
            emitter.onComplete();
        }).subscribe(System.out::println);
    }
}
