package scribbles.experiment3;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Random;

import static java.util.Arrays.asList;

@Slf4j
public class RestApi {
    private final Random random = new Random();
    private final List<String> adjectives = asList(
            "happy",
            "spooky",
            "green",
            "tired",
            "hungry",
            "lucky"
    );
    private final List<String> animals = asList(
            "horse",
            "dog",
            "cat",
            "elephant",
            "goat",
            "goose"
    );
    private final List<String> verbs = asList(
            "eating",
            "looking for",
            "looking at",
            "dreaming of",
            "excited about"
    );
    private final List<String> fruits = asList(
            "bananas",
            "apples",
            "grapes",
            "strawberries",
            "raspberries",
            "lemons"
    );
    private final List<String> weekdays = asList(
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday"
    );

    public String getAdjective() {
        performSeriousNetworking();
        return pickOneRandom(adjectives);
    }

    public String getAnimal() {
        performSeriousNetworking();
        return pickOneRandom(animals);
    }

    public String getVerb() {
        performSeriousNetworking();
        return pickOneRandom(verbs);
    }

    public String getFruit() {
        performSeriousNetworking();
        return pickOneRandom(fruits);
    }

    public String getWeekday() {
        performSeriousNetworking();
        return pickOneRandom(weekdays);
    }

    private <T> T pickOneRandom(List<T> elements) {
        return elements.get(random.nextInt(elements.size()));
    }

    private void performSeriousNetworking() {
        try {
            Thread.sleep(random.nextInt(2000));
        } catch (InterruptedException e) {
            log.error("nvm, {}", e.getMessage());
        }
    }
}
