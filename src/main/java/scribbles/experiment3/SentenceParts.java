package scribbles.experiment3;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SentenceParts {
    private final String adjective;
    private final String animal;
    private final String verb;
    private final String fruit;
    private final String weekday;
}
