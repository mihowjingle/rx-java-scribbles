package scribbles.experiment3;

public class SentenceBuilder {
    public String build(SentenceParts parts) {
        return parts.getAdjective() + " " + parts.getAnimal() + " is " + parts.getVerb()
                + " some " + parts.getFruit() + " on " + parts.getWeekday();
    }
}
