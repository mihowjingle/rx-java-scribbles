package scribbles.experiment3;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Slf4j
public class Experiment3 {
    private static final RestApi REST_API = new RestApi();
    private static final SentenceBuilder SENTENCE_BUILDER = new SentenceBuilder();
    private static final List<Supplier<String>> ENDPOINTS = asList(
            REST_API::getAdjective,
            REST_API::getAnimal,
            REST_API::getVerb,
            REST_API::getFruit,
            REST_API::getWeekday
    );

    /*

                  "await all"
    getAdjective |---->......| adjective \
    getAnimal    |----->.....| animal     \
    getVerb      |--->.......| verb        > SentenceParts | sentenceBuilder.build(sentenceParts) | subscribe(sentence) = log(sentence)
    getFruit     |---------->| fruit      /
    getWeekday   |------>....| weekday   /

     */
    public static void main(String[] args) {

        log.info("start");

        List<Single<String>> eventuallySentenceParts = ENDPOINTS.stream()
                .map(endpoint -> Single.<String>create(emitter -> {
                            String sentencePart = endpoint.get();
                            emitter.onSuccess(sentencePart);
                        }).subscribeOn(Schedulers.newThread())
                ).collect(Collectors.toList());

        Single.zip(eventuallySentenceParts, sentencePartsAsArray -> SentenceParts.builder()
                .adjective((String) sentencePartsAsArray[0])
                .animal((String) sentencePartsAsArray[1])
                .verb((String) sentencePartsAsArray[2])
                .fruit((String) sentencePartsAsArray[3])
                .weekday((String) sentencePartsAsArray[4])
                .build()
        ).blockingSubscribe(sentenceParts -> { // ahh, better, no need to sleep
            String sentence = SENTENCE_BUILDER.build(sentenceParts);
            log.info("{}", sentence);
        });

        log.info("end");
    }
}
