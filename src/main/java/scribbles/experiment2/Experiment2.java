package scribbles.experiment2;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Experiment2 {
    public static void main(String[] args) throws InterruptedException {
        log.info("start");
        Single<Integer> source1 = Single.<Integer>create(emitter -> {
            Thread.sleep(2000);
            emitter.onSuccess(2);
        }).subscribeOn(Schedulers.newThread());
        Single<Integer> source2 = Single.<Integer>create(emitter -> {
            Thread.sleep(3000);
            emitter.onSuccess(3);
        }).subscribeOn(Schedulers.newThread());
        Single.zip(source1, source2, Integer::sum).subscribe(
                sum -> log.info("Should print 5 after just three seconds: " + sum)
        );
        Thread.sleep(5000);
        log.info("end");
    }
}
